import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductController } from './product/product.controller';
import { ProductService } from './product/product.service';
import {ProductSchema} from './product/product.schema';
import {MongooseModule} from '@nestjs/mongoose';
import { UserService } from './user/user.service';
import { UserController } from './user/user.controller';
import { UserSchema} from './user/user.schema'
import { CustomerController } from './customer/customer.controller';
import { CustomerService } from './customer/customer.service';
import { CustomerSchema } from './customer/customer.schema';
import { PassportModule } from '@nestjs/passport';  

 
@Module({
  imports: [
    MongooseModule.forRoot('mongodb://127.0.0.1/products'),
    MongooseModule.forFeature([{name: 'products',schema:ProductSchema},{name: 'users',schema:UserSchema},{name: 'customers',schema:CustomerSchema}]),
    PassportModule.register({defaultStrategy:'jwt',session:false}),PassportModule,
    
  ],
  controllers: [AppController, ProductController, UserController, CustomerController],
  providers: [AppService, ProductService, UserService, CustomerService,],
})
export class AppModule {}
