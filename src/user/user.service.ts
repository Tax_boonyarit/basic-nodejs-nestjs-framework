import { Injectable } from '@nestjs/common';
import { UserAll } from './user.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import {UserCeateDto,UserUpdateDto} from './user.dto';

@Injectable()
export class UserService {
    constructor (@InjectModel('users')private readonly UserModel :Model<UserAll>){}

    async findAll(search:string): Promise<UserAll[]>{
        return await this.UserModel.find({"fristname": {'$regex': search}},{ __v : false}).exec();
    }
    async findId(id: string): Promise<UserAll>{
        return await this.UserModel.findById(id).exec();
    }
  
    createUser( data: UserCeateDto){
        return new this.UserModel(data).save();
    }
    updateUser( id: string, data : UserUpdateDto){
        return this.UserModel.updateOne({_id :id},data).exec();
    }
    deleteUser(id : string){
        return this.UserModel.deleteOne({_id:id}).exec();
    }
}
