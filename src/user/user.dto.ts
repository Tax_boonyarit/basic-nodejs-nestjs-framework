export  class UserCeateDto{
    readonly fristname: string;
    readonly lastname: string;
    readonly email: string;
    readonly age: number;
    readonly username: string;
    readonly password: string;
    readonly status: string;
}
export class UserUpdateDto{
    readonly fristname: string;
    readonly lastname: string;
    readonly email: string;
    readonly age: number;
    readonly password: string;
    readonly status: string;
}

export class UserLoginDto{
    readonly email: string;
    readonly password: string;
}