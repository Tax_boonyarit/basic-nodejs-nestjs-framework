import { Controller, Get, Put, Delete,Post, Body, Query, Param, UseGuards,Request } from '@nestjs/common';
import {UserService} from './user.service';
import {UserCeateDto,UserUpdateDto} from './user.dto';
import {UserAll} from './user.interface';
import * as  mongoose from 'mongoose';


@Controller('user')
export class UserController {
    constructor (private readonly userService: UserService){}

    @Get()
    async getUser(@Query ('search') search : string): Promise<UserAll[]>{
        return await this.userService.findAll(search);
    }
    @Get(':id')
    findeId(@Param('id') id: string): any{
        if(!mongoose.Types.ObjectId.isValid(id)){
            return { message : 'Object id failed.'}
        }
        return this.userService.findId(id);
    }
    
    @Post()
    async  createUser(@Body() body: UserCeateDto): Promise<any>{
        return await this.userService.createUser(body);
    }

    @Put(':id')
    async updateUser(@Param('id') id: string,@Body() body : UserUpdateDto): Promise<any>{
        if(!mongoose.Types.ObjectId.isValid(id)){
            return {message: 'Objece id failed.'}
        }
        return this.userService.updateUser(id,body);
    }
    @Delete(':id')
    async delete(@Param('id')id : string): Promise<any>{
            if(!mongoose.Types.ObjectId.isValid(id)){
                return {message : 'Object id fialed.'}
            }
            return this.userService.deleteUser(id);
        }


}
