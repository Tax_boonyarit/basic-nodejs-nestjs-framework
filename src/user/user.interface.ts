import {Document} from 'mongoose'

export interface UserAll extends Document {
    readonly _id: string;
    readonly fristname: string;
    readonly lastname: string;
    readonly email: string;
    readonly age: number;
    readonly username: string;
    readonly password: string;
    readonly status: string;
}
