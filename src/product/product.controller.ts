import { Controller,  Get,Post, Param, Delete, Put,Body, Query } from '@nestjs/common';
import * as mongoose from 'mongoose';
import {ProductService} from './product.service';
import {ProductAll} from './product.interface';
import {ProductCreateDto,ProductUpateDto} from './product.dto';

@Controller('product')
export class ProductController {
   constructor(private readonly productService: ProductService){}

    @Get()
    async getProduct(@Query('q')q:string): Promise<ProductAll[]>{
        return await this.productService.findAll(q);
    }
    @Get(':id')
    findOnce(@Param('id')id: string): any{
        if(!mongoose.Types.ObjectId.isValid(id)){
            return {message: 'Object id failed.'};
        }
        return this.productService.findId(id);
    }
    @Post()
    async createdproduct(@Body() body:ProductCreateDto): Promise<any>{
        
        return await this.productService.createProduct(body);
    }
    @Put(':id')
    async updateproduct(@Param('id') id: string,@Body() body:ProductUpateDto): Promise<any>{
        if(!mongoose.Types.ObjectId.isValid(id)){
            return {message: "Object id failed"};
        }
        return this.productService.updateProduct(id,body);
    }
    @Delete(':id')
    async  deleteproduct(@Param('id') id: string): Promise<any>{
        if (!mongoose.Types.ObjectId.isValid(id)){
            return { message: "Object id failed"};
        }
        return this.productService.deleteProduct(id);
    }
    
}



