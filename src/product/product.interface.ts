import {Document} from 'mongoose';

export interface ProductAll extends Document {
    readonly _id: string;
    readonly productname: string;
    readonly productype: string;
    readonly description: string;
    readonly price: number;
}
