import * as  mongoose from 'mongoose';

export const ProductSchema = new mongoose.Schema({
    productname: String,
    productype: String,
    description: String,
    price: Number,
});