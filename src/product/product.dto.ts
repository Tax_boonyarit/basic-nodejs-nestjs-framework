export class ProductCreateDto{
    readonly productname: string;
    readonly productype: string;
    readonly description: string;
    readonly price: number;
}
export class ProductUpateDto{
    readonly productname: string;
    readonly productype: string;
    readonly description: string;
    readonly price: number;
}