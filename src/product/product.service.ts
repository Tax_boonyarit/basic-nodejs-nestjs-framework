import { Injectable } from '@nestjs/common';
import {ProductAll} from './product.interface';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {ProductCreateDto,ProductUpateDto} from './product.dto';

@Injectable()
export class ProductService {
    constructor (@InjectModel('products')private readonly ProductModel : Model<ProductAll>){}

    async findAll(q:string) : Promise<ProductAll[]>{
        return await this.ProductModel
        .find({"productname":{'$regex':q}},{ __v : false})
        .exec();
    }
    async findId(id:string): Promise<ProductAll>{
      return await this.ProductModel.findById(id,{ __v : false}).exec();
    }
       createProduct (data: ProductCreateDto){
        return new this.ProductModel(data).save();
    }
     updateProduct (id:string , data : ProductUpateDto){
        return this.ProductModel.updateOne({_id : id },data).exec();
    }
     deleteProduct (id: string){
        return this.ProductModel.deleteOne({_id : id}).exec();
    }
}
