import * as  mongoose from 'mongoose';

export const CustomerSchema = new mongoose.Schema ({
    customername: String,
    customeraddress: String,
    customertel: String,
})