export class CustomerCreateDto {
    readonly  customername: string;
    readonly customeraddress: string;
    readonly customertel: string;
} 
export class CustomerUpdateDto {
    readonly  customername: string;
    readonly customeraddress: string;
    readonly customertel: string;
}