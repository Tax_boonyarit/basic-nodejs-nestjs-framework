import {Document} from 'mongoose'
export interface CustomerAll extends Document {
    readonly _id: string;
    readonly customername: string;
    readonly customeraddress: string;
    readonly customertel: string;
}
