import { Controller, Get, Put, Delete, Query, Param, Post, Body } from '@nestjs/common';
import {CustomerService} from './customer.service';
import {CustomerCreateDto,CustomerUpdateDto} from './customer.dto';
import {CustomerAll} from './customer.interface'
import * as mongoose from 'mongoose'
@Controller('customer')
export class CustomerController {
    constructor (private readonly customerService: CustomerService){}
            
    @Get()
    async getcutomer(@Query('q') q: string): Promise<CustomerAll[]>{
        return await this.customerService.findAll(q);
    }
    @Get(':id')
    getcutomerId(@Param('id') id :string): any{
        if(!mongoose.Types.ObjectId.isValid(id)){
            return {message : "object id failed"};
        }
        return this.customerService.findId(id);
    }
    @Post()
    async ceatecustomer(@Body() body: CustomerCreateDto): Promise<any>{
        return await this.customerService.createCustomer(body);
    }
    @Put(':id')
    async  updatecustomer(@Param('id') id: string, @Body() body : CustomerUpdateDto): Promise<any>{
        if(!mongoose.Types.ObjectId.isValid(id)){
            return {message : "object id failed."}
        }
        return this.customerService.updateCustomer(id,body);
    }
    @Delete(':id')
    async deleltecustomer(@Param('id') id: string): Promise<any>{
        return this.customerService.deleteCustomer(id);
    }
}
