import { Injectable } from '@nestjs/common';
import {CustomerAll} from './customer.interface';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import { CustomerCreateDto,CustomerUpdateDto} from './customer.dto'
@Injectable()
export class CustomerService {
  constructor (@InjectModel('customers') private readonly CustomerModel : Model<CustomerAll>){}

  async findAll(q: string): Promise<CustomerAll[]>{
      return await this.CustomerModel.find({"customername":{'$regex': q}}, {__v : false});
  }
  async findId(id :string): Promise<CustomerAll>{
      return await this.CustomerModel.findById(id,{ __v : false});
  }
  createCustomer (data : CustomerCreateDto){
      return new this.CustomerModel(data).save();
  }
  updateCustomer (id: string, data: CustomerUpdateDto){
      return this.CustomerModel.updateOne({_id: id},data,{ __v :false}).exec();
  }
  deleteCustomer (id: string){
      return this.CustomerModel.deleteOne({_id: id}).exec();
  }
}
